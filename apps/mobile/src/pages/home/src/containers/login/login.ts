import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { User } from '../../../../../../../api/both/models';

@Component({
  selector: 'festrr-login',
  templateUrl: 'login.html'
})
export class HomePageComponent {
  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {
  }

  login(authenticate: User) {
    console.log(authenticate);
  }
}
