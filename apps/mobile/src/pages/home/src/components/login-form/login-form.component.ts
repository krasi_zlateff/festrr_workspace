import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../../../../../../../api/both/models';

@Component({
  selector: 'festrr-login-form',
  templateUrl: 'login-form.component.html',
})
export class LoginFormComponent {
  @Output() submit = new EventEmitter<User>();

  constructor() {
  }

  signUpForm = new FormGroup({
    username: new FormControl('', [ Validators.required ]),
    password: new FormControl('', [ Validators.required ])
  });

  login() {
    this.submit.emit({
      username: this.signUpForm.value.username,
      password: this.signUpForm.value.password
    });
  }
}
