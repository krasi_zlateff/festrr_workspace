import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { environment } from '@festrr/env';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { storeFreeze } from 'ngrx-store-freeze';

import { MyAppComponent } from './app.component';
import { HomePageModule } from '../pages/home';

@NgModule({
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyAppComponent),
    StoreModule.forRoot({}, { metaReducers: !environment.production ? [ storeFreeze ] : [] }),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    HomePageModule
  ],
  declarations: [
    MyAppComponent
  ],
  bootstrap: [ IonicApp ],
  entryComponents: [
    MyAppComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {
}
